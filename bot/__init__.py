import json
import random
import string
from urllib.parse import urljoin

import requests

from bot import bot_config


class Bot(object):
    def __init__(self, bot_config, domain='127.0.0.1', port='8000'):
        self.domain = domain
        self.port = port
        self.bot_config = bot_config
        self.user_credentials = {}

    def run(self):
        """Run bot
        """

        self._create_user_credentials()
        self._signup_users()
        self._create_posts()
        self._add_likes()

    def _create_user_credentials(self):
        """Create user data
        """

        number_of_users = self.bot_config.get('number_of_users')
        for i in range(number_of_users):
            username = self._generate_random_text(100)
            password = self._generate_random_text(100)
            self.user_credentials[username] = password

    def _signup_users(self):
        """Signup users
        """

        for user_name, password in self.user_credentials.items():
            user_data = {
                'username': user_name,
                'password': password,
            }
            url = self._get_full_api_url('signup')
            response = requests.post(url, json=user_data)
            if response.status_code != 201:
                print('Error: {}: {}'.format(response.status_code, response.content))

    def _login_user(self, user_name, password):
        """Login user and return jwt token
        """

        user_data = {
            'username': user_name,
            'password': password,
        }
        url = self._get_full_api_url('token/')
        response = requests.post(url, json=user_data)
        if response.status_code != 200:
            print('Error: {}: {}'.format(response.status_code, response.content))
            return
        return response.json().get('access')

    def _get_full_api_url(self, url):
        """Get full url
        """

        return urljoin('http://{}:{}/api/'.format(self.domain, self.port), url)

    def _create_posts(self):
        """Creating random count of posts
        """

        max_posts_per_user = self.bot_config.get('max_posts_per_user')
        if not max_posts_per_user:
            return

        for user_name, password in self.user_credentials.items():
            # login user
            token = self._login_user(user_name, password)

            # post creating
            max_posts = random.randint(0, max_posts_per_user)
            for p in range(max_posts):
                url = self._get_full_api_url('posts')
                post_text = self._generate_random_text(100)
                headers = {'Authorization': 'Bearer {}'.format(token)}
                response = requests.post(url, headers=headers, json={'text': post_text})
                if response.status_code != 201:
                    print('Error: {}: {}'.format(response.status_code, response.content))

    def _generate_random_text(self, text_length):
        """Create random text
        """

        return ''.join(random.choice(string.ascii_lowercase) for x in range(random.randint(7, text_length)))

    def _add_likes(self):
        """Like post randomly
        """

        ids_of_all_posts = self._get_ids_of_posts()
        max_likes_per_user = self.bot_config.get('max_likes_per_user')

        for user_name, password in self.user_credentials.items():
            # login user
            token = self._login_user(user_name, password)

            headers = {'Authorization': 'Bearer {}'.format(token)}
            max_likes = random.randint(0, max_likes_per_user)
            for l in range(max_likes):
                post_id = random.choice(ids_of_all_posts)
                url = self._get_full_api_url('posts/{}/like'.format(post_id))
                response = requests.post(url, headers=headers)
                if response.status_code != 201:
                    print('Error: {}: {}'.format(response.status_code, response.content))

    def _get_ids_of_posts(self):
        """Get ids of all created posts
        """

        url = self._get_full_api_url('posts')
        response = requests.get(url)
        ids = [i['id'] for i in response.json()]
        return ids

