from rest_framework import routers

from social_network.apps.feed.api.views.post_view import PostViewSet, LikesViewSet
from social_network.apps.feed.api.views.signup_view import SignUpViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('posts', PostViewSet, base_name='feed-posts')
router.register('likes', LikesViewSet, base_name='feed-likes')
router.register('signup', SignUpViewSet, base_name='feed-signup')
