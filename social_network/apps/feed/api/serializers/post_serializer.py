from rest_framework import serializers

from social_network.apps.feed.models import Post


class PostSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'text', 'user_name')
        extra_kwargs = {'user_name': {'read_only': True},
                        'id': {'read_only': True},
                        }
