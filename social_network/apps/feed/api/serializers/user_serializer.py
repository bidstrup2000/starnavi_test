from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True},
                        'email': {'read_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = super().create(validated_data)
        user.set_password(password)
        user.save()
        return user
