from rest_framework import serializers

from social_network.apps.feed.models import Post, Like


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('post', 'user')
