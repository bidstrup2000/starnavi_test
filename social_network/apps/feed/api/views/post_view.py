from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from social_network.apps.feed.api.serializers.like_serializer import LikeSerializer
from social_network.apps.feed.api.serializers.post_serializer import PostSerializer
from social_network.apps.feed.models import Post, Like


class PostViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    serializer_class = PostSerializer
    permission_classes = [AllowAny, ]
    queryset = Post.objects.all()

    class Meta:
        model = Post

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.request.method == 'GET':
            self.permission_classes = [AllowAny, ]
        else:
            self.permission_classes = [IsAuthenticated, ]

        return super().get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=request.user)
        return Response(status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['POST'])
    def like(self, request, pk=None):
        post = self.get_object()
        Like.objects.create(user=request.user, post=post)
        return Response(status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['POST'])
    def unlike(self, request, pk=None):
        post = self.get_object()
        like = post.objects.filter(likes__user=request.user).first()
        if like:
            like.delete()
            return Response(status=status.HTTP_200_OK)


class LikesViewSet(mixins.CreateModelMixin,
                   mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = LikeSerializer
    queryset = Like.objects.all()

    class Meta:
        model = Like
