from django.contrib.auth.models import User
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny

from social_network.apps.feed.api.serializers.user_serializer import UserSerializer


class SignUpViewSet(mixins.CreateModelMixin,
               viewsets.GenericViewSet):
    serializer_class = UserSerializer
    permission_classes = [AllowAny,]

    class Meta:
        model = User
