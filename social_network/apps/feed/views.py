from django.db.models import Count
from django.views.generic import ListView

from social_network.apps.feed.models import Post


class PostsListView(ListView):
    model = Post
    queryset = Post.objects.select_related('user').annotate(likes_count=Count('likes')).all()
    allow_empty = True
