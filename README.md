# Test task


---
Installation:

**git clone https://bidstrup2000@bitbucket.org/bidstrup2000/test_task.git**

**python3 manage.py migrate**

To start the bot, please use:

**bot_run.py**

Config file: **bot/bot_config.py**

If you want to check results, run develop server and
follow this link:

**http://127.0.0.1:8000**

API docs:

**http://127.0.0.1:8000/docs/**


Optional tasks are not implemented (emailhunter and clearbit)