from bot import Bot, bot_config
from bot.config import WEBSITE_IP, WEBSITE_PORT


def run_bot():
    config = {
        'number_of_users': bot_config.number_of_users,
        'max_posts_per_user': bot_config.max_posts_per_user,
        'max_likes_per_user': bot_config.max_likes_per_user,
    }

    bot = Bot(config, WEBSITE_IP, WEBSITE_PORT)
    bot.run()


if __name__ == '__main__':
    run_bot()
